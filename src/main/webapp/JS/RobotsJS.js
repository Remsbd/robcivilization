var nbpopulation = 20000000000;
var massorganique = 50;
var nbmorts = 0;
var armeerobotic = 100;

// Compteur pour compter les années

var nbannee = 2080;
var victoire = false;

// Mise à jour Affichage, pour mettre à jour les variables

function miseAJourAffichage() {

	$("#nbpopulation").html(nbpopulation);
	$("#nbannee").html(nbannee);
	$("#nbmassorganic").html(massorganique);
	$("#nbmort").html(nbmorts);
	$("#nbrobots").html(armeerobotic);

}

// Fonction pour implémenter chaque année un nombre aléatoire d'humain

function naissance() {

	nbpopulation += (10050502 * Math.floor(Math.random() * 5));
	nbannee += 1;

	miseAJourAffichage();
	gagne();
	defaite();

	setTimeout(naissance, 5000);

}

// Fonction pour utiliser les bouttons tuer une partie de la population

function action() {

	linkbouton1 = $("#bouton1");
	linkbouton1.on("click", function() {

		var esclaves = 0;
		$("#photoarmy").css('display', "");
		alert("Invasion robotic en cours...");

		armeerobotic -= Math.floor(Math.random() * 20);
		esclaves += Math.floor(Math.random() * 100);
		alert("Robots restants : " + armeerobotic);
		alert("Vous avez capture: " + esclaves + " Humains");
		alert("Conversion en masse organique...");
		$("#photoarmy").css('display', "none");

		massorganique += Math.floor(esclaves * 10);
		nbmorts += (10 * Math.floor(Math.random() * 999));
		nbpopulation -= nbmorts;

		miseAJourAffichage();
		gagne();
		defaite();

	});

	linkbouton2 = $("#bouton2");

	linkbouton2.on("click", function() {
		if (massorganique >= 50) {
			$("#missile1").css('display', "");

			alert("Lancement missiles en cours...");

			massorganique -= 50;
			nbmorts += (10 * Math.floor(Math.random() * 99999));
			nbpopulation -= nbmorts;
			alert("Les lancements ont faits: " + nbmorts + " morts");
			$("#missile1").css('display', "none");
			miseAJourAffichage();
			gagne();
			defaite();

		} else {
			alert("Pas assez de massOrganique")
		}

	});

	linkbouton3 = $("#bouton3");

	linkbouton3.on("click", function() {
		if (massorganique >= 1000) {
			$("#photoatomic").css('display', "");
			alert("Envoi ogive nucléaire ...");

			massorganique -= 1000;
			nbmorts += (10 * Math.floor(Math.random() * 999999999));
			nbpopulation -= nbmorts;
			alert("Les ogives ont faits: " + nbmorts + " morts");
			$("#photoatomic").css('display', "none");
			miseAJourAffichage();
			gagne();
			defaite();
		} else {
			alert("Pas assez de massOrganique")
		}
	});

}

// Function gagner la partie
function gagne() {

	if (nbpopulation <= 0) {
		alert("VICTOIRE !!");
		alert("L'humanite a ete aneantie !");
		$("#buttonattack").css('display', "none");
		$("#donnedeguerre").css('display', "none");

		$("#action").css('display', "none");
		$("body").css({
			'background' : 'url("Images/robotsvictory.gif")',
			'background-size' : '100%'
		});

	}

}

// Function défaite
function defaite() {

	if (armeerobotic <= 0) {
		alert("DEFAITE !!");
		alert("Votre armee a ete aneantie !");

		$("#buttonattack").css('display', "none");
		$("#donnedeguerre").css('display', "none");
		$("#action").css('display', "none");

		$("body").css({
			'background' : 'url("Images/defaite1.gif")',
			'background-size' : '100%'
		});
	}

	else if (nbpopulation >= 20100000000) {

		alert("DEFAITE !!");
		alert("Surpopulation !");
		$("#buttonattack").css('display', "none");
		$("#donnedeguerre").css('display', "none");
		$("#action").css('display', "none");

		$("body").css({
			'background' : 'url("Images/population.gif")',
			'background-size' : '100%'
		});
	} else if (massorganique <= 0) {

		alert("DEFAITE !!");
		alert("MassOrganique epuise !");
		$("#buttonattack").css('display', "none");
		$("#donnedeguerre").css('display', "none");
		$("#action").css('display', "none");

		$("body").css({
			'background' : 'url("Images/hsrobots.gif")',
			'background-size' : '100%'
		});
	}

}

// A faire.... rajouter une page HTML avec "Rentrer un pseudo" une fois clique
// sur envoyé, ouvrir la page de jeu.
// Stocker les donnees dans une troisieme page pour liste de temps et
// enregistrer les scores...

// Fonction main

function main() {

	naissance();
	action();
}
