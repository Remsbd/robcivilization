<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>1erPage</title>
</head>
<body>

	<form method="post" action="FirstServlets">

		<label for="nom">Nom :</label> <input type="text" name="nom" id="nom">
		<input type="submit" />

		<c:if test="${!empty nom}">

			<div>
				<c:out value="Bonjour ${nom}" />

				<div>
					<a href="SecondPage.jsp">Lancer le Jeu</a>
				</div>

			</div>

		</c:if>

	</form>
	<script src="JS/jquery-2.1.4.min.js"></script>

</body>

</html>