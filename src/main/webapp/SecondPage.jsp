<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <%@ page isELIgnored="false" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Destruction du monde</title>
<link rel="stylesheet" type="text/css" href="CSS/RobotsCSS.css">
</head>

<body onload="main();">

	<div id="donnedeguerre">

		<div id="populationhumain">
			Population humaine: <span id="nbpopulation">200000000</span>
		</div>
		<div id="annee">
			Année: <span id="nbannee">2079</span>
		</div>
		<div id="massorganic">
			Masse Organique: <span id="nbmassorganic">50</span>
		</div>
		<div id="morts">
			Mort: <span id="nbmort">0</span>
		</div>
		<div id="Armee">
			Votre Armee : <span id="nbrobots"></span>
		</div>
		<div id="pseudogame">
			Nom JOUEUR : <span id="nameJoueur"> </span>
		</div>
		
		<div id="Cercle1"></div>
		<div id="Cercle2"></div>
		<div id="Cercle3"></div>
		<div id="Cercle4"></div>
		<div id="Cercle5"></div>

	</div>

	<div id="interface">

		<div id="buttonattack">

			<div id="bouton1">
				<button id="buttonarmy">Robots Attack (gratuit)</button>
			</div>

			<div id="bouton2">
				<button id="buttonpandemic">Missils Attack (50 Masse
					Organique)</button>
			</div>

			<div id="bouton3">
				<button id="buttonatomic">Atomic Attack (1000 Masse
					Organique)</button>
			</div>

		</div>

		<div id="action">

			<img id="missile1" style="display: none" src="Images/missile1.gif"
				alt="missile"> 
				<img id="photoarmy" style="display: none"src="Images/robotarmy.gif" alt="robot"> 
				<img id="photoatomic" style="display: none" src=Images/atomic1.gif alt="bombeatomic">

		</div>

	</div>

	<script src="JS/jquery-2.1.4.min.js">
		
	</script>
	<script type="text/javascript" src="JS/RobotsJS.js"></script>
</body>



</html>